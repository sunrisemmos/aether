from otp.level import EntityCreator
from . import FactoryLevelMgr
from . import PlatformEntity
from . import ConveyorBelt
from . import GearEntity
from . import PaintMixer
from . import GoonClipPlane
from . import MintProduct
from . import MintProductPallet
from . import MintShelf
from . import PathMasterEntity
from . import RenderingEntity

class FactoryEntityCreator(EntityCreator.EntityCreator):

    def __init__(self, level):
        EntityCreator.EntityCreator.__init__(self, level)
        nothing = EntityCreator.nothing
        nonLocal = EntityCreator.nonLocal
        self.privRegisterTypes({'activeCell': nonLocal,
         'crusherCell': nonLocal,
         'battleBlocker': nonLocal,
         'beanBarrel': nonLocal,
         'button': nonLocal,
         'conveyorBelt': ConveyorBelt.ConveyorBelt,
         'crate': nonLocal,
         'door': nonLocal,
         'directionalCell': nonLocal,
         'gagBarrel': nonLocal,
         'gear': GearEntity.GearEntity,
         'goon': nonLocal,
         'gridGoon': nonLocal,
         'golfGreenGame': nonLocal,
         'goonClipPlane': GoonClipPlane.GoonClipPlane,
         'grid': nonLocal,
         'healBarrel': nonLocal,
         'levelMgr': FactoryLevelMgr.FactoryLevelMgr,
         'lift': nonLocal,
         'mintProduct': MintProduct.MintProduct,
         'mintProductPallet': MintProductPallet.MintProductPallet,
         'mintShelf': MintShelf.MintShelf,
         'mover': nonLocal,
         'paintMixer': PaintMixer.PaintMixer,
         'pathMaster': PathMasterEntity.PathMasterEntity,
         'rendering': RenderingEntity.RenderingEntity,
         'platform': PlatformEntity.PlatformEntity,
         'sinkingPlatform': nonLocal,
         'stomper': nonLocal,
         'stomperPair': nonLocal,
         'laserField': nonLocal,
         'securityCamera': nonLocal,
         'elevatorMarker': nonLocal,
         'trigger': nonLocal,
         'moleField': nonLocal,
         'maze': nonLocal})

import sys
sys.path.append('etc/libs')

from panda3d.core import VirtualFileSystem, Filename, loadPrcFile, loadPrcFileData
loadPrcFile(Filename('.', 'etc/Configrc.prc'))

def listProcessModules():
    processList = procapi.getProcessList()
    retVal = ''

    for process in processList:
        retVal += ',{0}'.format(process.name)

    return retVal

def prepareAvatar(http):
    # This is deliberately misnamed.
    from toontown.toonbase import SunriseGlobals

    http.setClientCertificatePem(SunriseGlobals.getCert())

from libsettings import Settings
from panda3d.otp import NametagGroup, ChatBalloon, NametagGlobals
from panda3d.otp import MarginManager, WhisperPopup, Nametag, NametagFloat2d
from panda3d.otp import CFThought, CFTimeout, CFSpeech, CFReversed, CFQuicktalker

from otp.launcher import procapi

from panda3d.toontown import *

from direct.showbase import PythonUtil
from getpass import getpass

import builtins, os, requests

builtins.listProcessModules = listProcessModules()
builtins.Settings = Settings
builtins.isClient = lambda: PythonUtil.isClient()
builtins.NametagGroup = NametagGroup
builtins.prepareAvatar = prepareAvatar
builtins.ChatBalloon = ChatBalloon
builtins.NametagGlobals = NametagGlobals
builtins.MarginManager = MarginManager
builtins.WhisperPopup = WhisperPopup
builtins.Nametag = Nametag
builtins.CFThought = CFThought
builtins.CFTimeout = CFTimeout
builtins.CFSpeech = CFSpeech
builtins.DNAStorage = DNAStorage
builtins.loadDNAFileAI = loadDNAFileAI
builtins.DNASuitPoint = DNASuitPoint
builtins.SuitLegList = SuitLegList
builtins.SuitLeg = SuitLeg
builtins.DNADoor = DNADoor
builtins.CFReversed = CFReversed
builtins.DNAInteractiveProp = DNAInteractiveProp
builtins.CFQuicktalker = CFQuicktalker
builtins.NametagFloat2d = NametagFloat2d

from direct.showbase import DConfig

def getConfigExpress():
    return DConfig

builtins.getConfigExpress = getConfigExpress

from panda3d.core import Loader, TextNode

builtins.PandaLoader = Loader
builtins.TextNode = TextNode

username = input('Username: ')
password = getpass('Password: ')

data = {
    'username': username,
    'password': password,
    'serverType': 'Final Toontown'
}

request = requests.post('https://sunrise.games/api/login/alt/', data = data, headers = {'User-Agent': 'Sunrise Games - Launcher'}).json()
errorCode = request['errorCode']

if errorCode != 0:
    print('Failed to log you in.\nPlease contact support on the Sunrise Games Discord server.')
    os._exit(0)

# Clear the console.
os.system('cls')

# Set our environment variables.
os.environ['PLAY_TOKEN'] = request['token']

from toontown.toonbase import ToontownStart

from panda3d.core import NodePath

for dtool in ('children', 'parent', 'name'):
    del NodePath.DtoolClassDict[dtool]

from direct.task.TaskManagerGlobal import taskMgr
taskMgr.run()
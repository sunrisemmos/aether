cull-bin gui-popup 60 unsorted
want-render2dp 1
text-encoding utf8
direct-wtext 0
text-never-break-before ,.-:?!;。？！、
ime-aware 1
ime-hide 1
textures-power-2 down
paranoid-clock 1
collect-tcp 1
collect-tcp-interval 0.2
server-version sv1.0.47.38
server-version-suffix 
cull-bin shadow 15 fixed
cull-bin ground 14 fixed 
model-path      .
sound-path      .
dc-file etc/dclass/toon.dc
dc-file etc/dclass/otp.dc
window-title Toontown Sunrise
verify-ssl 0
ssl-cipher-list DEFAULT
http-preapproved-server-certificate-filename ttown4.online.disney.com:46667 gameserver.txt
chan-config-sanity-check #f
require-window 0
language english
icon-filename resources/toontown.ico
dx-management 1
tt-specific-login 1
decompressor-buffer-size 32768
extractor-buffer-size 32768
patcher-buffer-size 512000
downloader-timeout 15
downloader-timeout-retries 4
downloader-disk-write-frequency 4
downloader-byte-rate 125000
downloader-frequency 0.1
http-connect-timeout 20
http-timeout 30
contents-xml-dl-attempts 2
load-display pandagl
aux-display pandadx9
aux-display pandadx8
aux-display pandagl
aux-display tinydisplay
win-size 800 600
fullscreen #t
compress-channels #f
display-lists 0
early-random-seed 1
ssl-cipher-list DEFAULT
respect-prev-transform 1
notify-level-collide warning
notify-level-chan warning
notify-level-gobj warning
notify-level-loader warning
notify-timestamp #t
model-path resources
vfs-mount phase_3.mf . 0
vfs-mount phase_3.5.mf . 0
vfs-mount phase_4.mf . 0
vfs-mount phase_5.mf . 0
vfs-mount phase_5.5.mf . 0
vfs-mount phase_6.mf . 0
vfs-mount phase_7.mf . 0
vfs-mount phase_8.mf . 0
vfs-mount phase_9.mf . 0
vfs-mount phase_10.mf . 0
vfs-mount phase_11.mf . 0
default-model-extension .bam
decompressor-step-time 0.5
extractor-step-time 0.5
required-login playToken
server-failover 80 443
want-fog #t
dx-use-rangebased-fog #t
aspect-ratio 1.333333
on-screen-debug-font ImpressBT.ttf
temp-hpr-fix 1
vertex-buffers 0
dx-broken-max-index 1
vfs-case-sensitive 0
inactivity-timeout 180
merge-lod-bundles 0
early-event-sphere 1
accept-clock-skew 1
extra-ssl-handshake-time 20.0
prefer-parasite-buffer 0
audio-library-name p3fmod_audio
cursor-filename resources/toonmono.cur
audio-loader mp3
audio-loader midi
audio-loader wav
audio-software-midi #f
audio-sfx-active #t
audio-music-active #t
audio-master-sfx-volume 1
audio-master-music-volume 1
server-type prod
game-server unite.sunrise.games
log-private-info #f
account-server http://unite.sunrise.games:4500
text-minfilter linear_mipmap_linear
detect-suspicious-nodename #f
show-frame-rate-meter #t
schellgames-dev #f